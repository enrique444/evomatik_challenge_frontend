import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { Pelicula } from '../clases/pelicula';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.css']
})
export class PeliculaComponent implements OnInit {
  peliculas: Pelicula[];
  peliculaNueva: Pelicula;
  peliculaAModificar: Pelicula;
  indice: number;

  constructor(private restApi: RestService) { 
    this.peliculaNueva = new Pelicula();
    this.peliculaNueva.nombre = "";
    this.peliculaNueva.descripcion = "";
    this.peliculaAModificar = new Pelicula();
    this.peliculaAModificar.nombre = "";
    this.peliculaAModificar.descripcion = "";
  }

  ngOnInit() {
    this.obtenerListaDePeliculas();
  }

  obtenerListaDePeliculas(): void{
    this.restApi.obtenerTodasLasPeliculas()
    .subscribe(peliculas => this.peliculas = peliculas);
  }

  crearPelicula(): void{
    this.restApi.crearNuevaPelicula(this.peliculaNueva)
      .subscribe(peliculaCreada => {
        this.peliculas.push(peliculaCreada);
      });
  }

  eliminarPelicula(peliculaAEliminar: Pelicula): void{
    this.restApi.eliminarPelicula(peliculaAEliminar).subscribe((r) => {
      this.peliculas = this.peliculas.filter(p => p !== peliculaAEliminar);
    });
  }

  modificarPelicula(): void{
    this.restApi.modificarPelicula(this.peliculaAModificar).subscribe((r) => {
      this.peliculas[this.indice] = r;
      document.getElementById("btnModalCancel").click();
    });
  }

  modoEditarPelicula(pelicula: Pelicula, index: number): void{
    this.indice = index;
    this.peliculaAModificar = JSON.parse(JSON.stringify(pelicula));
  }

}
