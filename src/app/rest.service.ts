import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Pelicula } from './clases/pelicula';
import { Usuario } from './clases/usuario';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class RestService {
  private baseUrl = "http://localhost:8080/";
  constructor(private http: HttpClient) { }

  obtenerTodasLasPeliculas (): Observable<Pelicula[]> {
    var url = this.baseUrl + "pelicula"
    return this.http.get<Pelicula[]>(url);
  }

  crearNuevaPelicula (pelicula: Pelicula): Observable<Pelicula> {
    var url = this.baseUrl + "pelicula"
    return this.http.post<Pelicula>(url, pelicula, httpOptions).pipe(
      tap((pelicula: Pelicula) => this.log(`pelicula agregada w/ id=${pelicula.id}`)),
      catchError(this.handleError<Pelicula>('crearNuevaPelicula'))
    );
  }

  eliminarPelicula (pelicula: Pelicula): Observable<Pelicula> {
    var url = this.baseUrl + "pelicula/" + pelicula.id;
    return this.http.delete<Pelicula>(url, httpOptions).pipe(
      tap((pelicula: Pelicula) => this.log(` pelicula eliminada w/ id=${pelicula.id}`)),
      catchError(this.handleError<Pelicula>('eliminarPelicula'))
    );
  }

  modificarPelicula(pelicula: Pelicula): Observable<Pelicula> {
    var url = this.baseUrl + "pelicula/" + pelicula.id;
    return this.http.put<Pelicula>(url, pelicula, httpOptions).pipe(
      tap((pelicula: Pelicula) => this.log(`pelicula modificada w/ id=${pelicula.id}`)),
      catchError(this.handleError<Pelicula>('modificarPelicula'))
    );
  }

  rentarPelicula (pelicula: Pelicula): Observable<Pelicula> {
    var url = this.baseUrl + "pelicula/rentar"
    return this.http.post<Pelicula>(url, pelicula, httpOptions).pipe(
      tap((pelicula: Pelicula) => this.log(`pelicula rentada w/ id=${pelicula.id}`)),
      catchError(this.handleError<Pelicula>('rentarPelicula'))
    );
  }

  devolverPelicula (pelicula: Pelicula): Observable<Pelicula> {
    var url = this.baseUrl + "pelicula/devolver"
    return this.http.post<Pelicula>(url, pelicula, httpOptions).pipe(
      tap((pelicula: Pelicula) => this.log(`pelicula devuelta w/ id=${pelicula.id}`)),
      catchError(this.handleError<Pelicula>('devolerPelicula'))
    );
  }

  obtenerTodosLosUsuarios (): Observable<Usuario[]> {
    var url = this.baseUrl + "usuario"
    return this.http.get<Usuario[]>(url);
  }

  crearNuevoUsuario (usuario: Usuario): Observable<Usuario> {
    var url = this.baseUrl + "usuario"
    return this.http.post<Usuario>(url, usuario, httpOptions).pipe(
      tap((usuario: Usuario) => this.log(`agregado usuario w/ id=${usuario.id}`)),
      catchError(this.handleError<Usuario>('crearNuevoUsuario'))
    );
  }

  eliminarUsuario (usuario: Usuario): Observable<Usuario> {
    var url = this.baseUrl + "usuario/" + usuario.id;
    return this.http.delete<Usuario>(url, httpOptions).pipe(
      tap((usuario: Usuario) => this.log(`usuario eliminado w/ id=${usuario.id}`)),
      catchError(this.handleError<Usuario>('eliminarUsuario'))
    );
  }

  modificarUsuario(usuario: Usuario): Observable<Usuario> {
    var url = this.baseUrl + "usuario/" + usuario.id;
    return this.http.put<Usuario>(url, usuario, httpOptions).pipe(
      tap((usuario: Usuario) => this.log(`usuario modificado w/ id=${usuario.id}`)),
      catchError(this.handleError<Usuario>('modificarUsuario'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
        console.error(error);
       this.log(`${operation} failed: ${error.message}`);
       return of(result as T);
    };
  }
 
  private log(message: string) {
    console.log(message);
  }

}
