import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PeliculaComponent} from './pelicula/pelicula.component';
import {UsuarioComponent} from './usuario/usuario.component';
import {RentaComponent} from './renta/renta.component';

const routes: Routes = [
  { path: '', redirectTo: '/rentas', pathMatch: 'full' },
  { path: 'peliculas', component: PeliculaComponent },
  { path: 'usuarios', component: UsuarioComponent },
  { path: 'rentas', component: RentaComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
