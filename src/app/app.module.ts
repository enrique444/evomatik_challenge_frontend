import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { NavegacionComponent } from './navegacion/navegacion.component';
import { PeliculaComponent } from './pelicula/pelicula.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { RentaComponent } from './renta/renta.component';
import { RestService } from './rest.service';


@NgModule({
  declarations: [
    AppComponent,
    NavegacionComponent,
    PeliculaComponent,
    UsuarioComponent,
    RentaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [RestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
