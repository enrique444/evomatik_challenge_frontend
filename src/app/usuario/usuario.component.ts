import { Component, OnInit } from '@angular/core';
import { Usuario } from '../clases/usuario';
import { RestService } from '../rest.service';


@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  usuarios: Usuario[];
  usuarioNuevo: Usuario;
  usuarioAModificar: Usuario;
  indice: number;

  constructor(private restApi: RestService ) { 
    this.usuarioNuevo = new Usuario();
    this.usuarioAModificar = new Usuario();
  }

  ngOnInit() {
    this.obtenerListaDeUsuarios();
  }

  obtenerListaDeUsuarios(): void{
    this.restApi.obtenerTodosLosUsuarios()
    .subscribe(usuarios => this.usuarios = usuarios);
  }

  crearUsuario(): void{
    this.restApi.crearNuevoUsuario(this.usuarioNuevo)
      .subscribe(usuarioCreado => {
        this.usuarios.push(usuarioCreado);
      });
  }

  eliminarUsuario(usuarioAEliminar: Usuario): void{
    this.restApi.eliminarUsuario(usuarioAEliminar).subscribe((r) => {
      this.usuarios = this.usuarios.filter(p => p !== usuarioAEliminar);
    });
  }

  modificarUsuario(): void{
    this.restApi.modificarUsuario(this.usuarioAModificar).subscribe((r) => {
      this.usuarios[this.indice] = r;
      document.getElementById("btnModalCancel").click();
    });
  }

  modoEditarUsuario(usuario: Usuario, index: number): void{
    this.indice = index;
    this.usuarioAModificar = JSON.parse(JSON.stringify(usuario));
  }

}
