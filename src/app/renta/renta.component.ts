import { Component, OnInit } from '@angular/core';
import { Pelicula } from '../clases/pelicula';
import { RestService } from '../rest.service';
import { Usuario } from '../clases/usuario';

@Component({
  selector: 'app-renta',
  templateUrl: './renta.component.html',
  styleUrls: ['./renta.component.css']
})
export class RentaComponent implements OnInit {
  peliculas: Pelicula[];
  usuarios: Usuario[];
  usuarioQueRenta: Usuario;
  indicePelicula: number;
  peliculaARentar: Pelicula;
  peliculaADevolver: Pelicula;

  constructor(private restApi: RestService) { }

  ngOnInit() {
    this.obtenerListaDePeliculas();
    this.obtenerListaDeUsuarios();
  }

  obtenerListaDePeliculas(): void{
    this.restApi.obtenerTodasLasPeliculas()
    .subscribe(peliculas => this.peliculas = peliculas);
  }

  obtenerListaDeUsuarios(): void{
    this.restApi.obtenerTodosLosUsuarios()
    .subscribe(usuarios => this.usuarios = usuarios);
  }

  modoRentarPelicula(pelicula: Pelicula, index: number): void{
    this.indicePelicula = index;
    this.peliculaARentar = JSON.parse(JSON.stringify(pelicula));
  }

  rentarPelicula(): void{
    if(this.usuarioQueRenta){
      this.peliculaARentar.arrendatario = this.usuarioQueRenta;
      this.restApi.rentarPelicula(this.peliculaARentar).subscribe((r) => {
        this.peliculas[this.indicePelicula] = r;
        this.usuarioQueRenta = null;
        this.obtenerListaDeUsuarios();
        document.getElementById("btnModalCancel").click();
      });
    }else{
      document.getElementById("btnModalCancel").click();
    }
  }

  devolverPelicula(pelicula: Pelicula, index: number): void{
    this.restApi.devolverPelicula(pelicula).subscribe((r) => {
      console.log(this.usuarioQueRenta);
      this.peliculas[index] = r;
    });
  }

}
