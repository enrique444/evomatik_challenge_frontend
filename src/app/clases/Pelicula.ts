import {Usuario} from "./usuario"

export class Pelicula {
  id: number;
  nombre: string;
  descripcion: string;
  arrendatario: Usuario;
}