import {Pelicula} from "./pelicula"

export class Usuario {
  id: number;
  nombre: string;
  direccion: string;
  rentasTotales: number;
  peliculasRentadas: Pelicula[];

  constructor(){
    this.nombre = "";
    this.direccion = "";
    this.rentasTotales = 0;
  }
}